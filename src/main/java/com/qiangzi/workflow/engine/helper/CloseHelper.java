package com.qiangzi.workflow.engine.helper;

import java.io.Closeable;

public class CloseHelper {

	public static void close(Closeable obj) {

		if (null != obj) {
			try {
				obj.close();
			} catch (Exception e) {
			}
		}

	}
}
