package com.qiangzi.workflow.engine.core;

import org.springframework.core.io.Resource;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;

public interface Engine {

	public ProcessDefinition parse(Resource resource) throws Exception;

	public ProcessInstance deploy(ProcessDefinition processDefinition) throws Exception;

	public void run(ProcessInstance processInstance) throws Exception;
}
