package com.qiangzi.workflow.engine.core;

import com.qiangzi.workflow.engine.bpmn.base.Action;
import com.qiangzi.workflow.engine.bpmn.base.Node;

import lombok.Data;

@Data
public class NodeExecution {

	private Node node;
	private Action action;

}
