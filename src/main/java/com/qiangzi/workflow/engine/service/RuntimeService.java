package com.qiangzi.workflow.engine.service;

import org.springframework.context.ApplicationContext;

import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;

public interface RuntimeService {

    public void run(ProcessInstance instance,
                    ApplicationContext applicationContext) throws Exception;

}
