package com.qiangzi.workflow.engine.bpmn.sentinel;

import org.springframework.context.ApplicationContext;

import com.qiangzi.workflow.engine.bpmn.base.Node;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;

public class SentinelNode extends Node {

    @Override
    public Object copy() {
        // SentinelNode node = new SentinelNode();
        // super.copy(node);
        // return node;
        return null;
    }

    @Override
    public void invoke(ProcessInstance instance,
                       ApplicationContext applicationContext) throws Exception {
        throw new Exception("not supported operation");
    }

}
