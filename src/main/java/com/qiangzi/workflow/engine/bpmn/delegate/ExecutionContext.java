package com.qiangzi.workflow.engine.bpmn.delegate;

import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;

public class ExecutionContext implements DelegateExecution {

    private ProcessInstance processInstance;

    public ExecutionContext(ProcessInstance inst) {

        processInstance = inst;

    }

    @SuppressWarnings("rawtypes")
    @Override
    public void setVariable(String varName, Object value, Class valueClass) throws Exception {

        processInstance.setVariable(varName, value, valueClass);

    }

    @Override
    public Object getVariable(String varName) {

        Object obj = processInstance.getVariable(varName);

        if (null == obj) {
            throw new NullPointerException("no result for " + varName);
        }
        return obj;

    }

}
